//: [Previous](@previous)

/*:
 ## Higher Order Functions
 A higher order function is a function that takes a function as an argument, or returns a function. There are many higher order functions, but we are going to look at three of the most common: `map`, `reduce`, and `filter`
 
 ___
 
 #### Map
 Perform a given function on every element in a collection.
 
 `public func map<T>(_ transform: (Element) throws -> T) rethrows -> [T]`
 */
import Foundation

let ar = [1, 2, 3, 4]
print("Result of map: \( ar.map {val in val + 3} )")


// Let's make our own map
extension Array {
  // T is the output type
  func myMap<T>(_ transform: (Element) -> T) -> [T] {
    var result: [T] = []

    for item in self {
      result.append(transform(item))
    }

    return result
  }
}

print("Result of myMap: \(ar.myMap { $0  + 3 })")
/*:
 [Here is the swift implementation of map](https://github.com/apple/swift/blob/main/stdlib/public/core/Collection.swift)
 */
/*:
 #### Reduce
 The reduce() method reduces an array of values down to just one value. You pass a "starts with" value, along with a closure on how to reduce the array.
 */
print("Result of reduce: \(ar.reduce(0) { $0 + $1 })")
/*:
 #### Filter
 The filter() method takes each element in an array and it applies a conditional statement against it. If this conditional returns true, the element gets pushed to the output array. If the condition returns false, the element does not get pushed to the output array.
 */
print("Result of filter: \(ar.filter { $0 % 2 == 0 })")
/*:
 #### Challenge
 Let's make a function that returns a function!
 */
/// Generate the n largest numbers from a given array
/// - Parameter arr: Unsorted array of Comparable T
/// - Returns: A function, that given a number <n>, will return the the <n> greatest numbers of `arr`
func getNLargest<T: Comparable> (arr: [T]) -> ((Int) -> [T]) {
    return  { val in
        let sortedArr = Array(arr.sorted().reversed())
        return Array(sortedArr[..<val])
    }
}

let values = [2, 7, 8, 1, 4]
let function = getNLargest(arr: values)
function(3) // [8, 7, 4]
//: [Next](@next)
