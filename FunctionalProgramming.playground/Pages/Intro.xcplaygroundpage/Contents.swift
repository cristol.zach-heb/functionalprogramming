//: [Previous](@previous)
/*:
 # Functional Programming
 
 - [Definitions](Definitions)
 - [Examples](Examples)
 - [Higher Order Functions](HigherOrderFunctions)
 
 
*/
//: [Next](@next)
