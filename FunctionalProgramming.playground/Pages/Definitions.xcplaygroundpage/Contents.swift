//: [Previous](@previous)
/*:
 # Functional Programming
 
 
 - Note:
  "Functional programming is a programming paradigm where logic is implemented by applying and composing functions. In this way, functional programming is a declarative type of programming style."

 ___
 ## Core Tenants of Functional Programming
 
 ### Declarative vs Imperative
 - **Declarative Programming**:
    - Describes what should happen, but not necessarily how it should be executed.
    - In SQL you might see:
    ```
    SELECT * FROM Fruits WHERE Fruit_Color='Red'
    ```
    - We see a description of what needs to be done, but not an explicit implementation on how to do it.
 - **Imperative Programming**: 
    - Describes how something should happen, and causes a change of the program state.
    - We might see a similar program described as such:
    ```
    let red_fruits = []
    for fruit in fruits:
        if fruit.color == red:
            red_fruits.append(fruit)
    ```
 
 ### Pure Functions
 A pure function must have these two properties:
 1. The function returns the same values, given the same input. No dependence on static or global variables that may change.
 2. The function has no side effects. There is no manipulation of static or global variables.
 
 ### Referential transparency
 Variables once defined in a functional programming language aren’t allowed to change the value that they are holding throughout the execution of the program. This is known as referential transparency. It assures that the same language expression gives the same output.
 
 In Swift, this is effectively using `let` over `var`, enforcing that our variables are immutable. Other languages like C++ call these immutable variables `const`.
 
 Referential transparency eliminates even the slightest chances of any undesired effects due to the fact that any variable can be replaced with its actual value during any point in the program execution.
 
 ### Recursion
 
 In the functional programming paradigm, there is no for and while loops. Instead, functional programming languages rely on recursion for iteration. Recursion is implemented using recursive functions, which repetitively call themselves until the base case is reached. The use of this allows us to avoid mutable state when executing a program.
 
 ### Functions are First-Class and can be Higher-Order
 - First-class functions are treated as first-class variable. The first class variables can be passed to functions as parameter, can be returned from functions or stored in data structures (so the same must apply to a functional function).
 - Higher order functions are the functions that take other functions as arguments and they can also return functions.
 
 ### Variables are Immutable
 
 In functional programming, we can’t modify a variable after it’s been initialized. We can create new variables – but we can’t modify existing variables, and this really helps to maintain state throughout the runtime of a program. Once we create a variable and set its value, we can have full confidence knowing that the value of that variable will never change.
 
 This means when changing or manipulating variables or data structures, new copies are made. Thanks to copy-on-write semantics, this does not yield a large memory performance hit.
 
 ___
 
 ## Advantages
 - Pure functions are easier to reason about. Their function signature gives all the information about them i.e. their return type and their arguments.
 - Testing and debugging is easier. We can be more confident that the use of a tested function won't cause adverse effects.
 - Safer to implement concurrency/parallelism because pure functions don’t change variables or any other data outside of it.
 
 ## Disadvantages
 - Writing functional programs can be a hurdle if it is a new way of thinking.
 - Immutable values and recursion can lead to decrease in performance.
 - Writing pure functions are easy but combining them with rest of application and I/O operations is the difficult task.
 
 
*/
//: [Next](@next)
