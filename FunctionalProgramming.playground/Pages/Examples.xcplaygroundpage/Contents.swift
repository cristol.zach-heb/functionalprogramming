//: [Previous](@previous)
/*:
 # Pure Function Example
 Here we see two impure functions. If these functions were to use async programming, we could hit a race condition. Although the two function calls are ordered, the computation of the second function call could complete before the first one, affecting the results of the first call before it has finished.
*/
import Foundation

var x = 10

func square () {
    x = x * x
}
func addOne () {
    x = x + 1
}


// We see that a different ordering returns a different result. If these tasks were async and can be completed
// at different times, we can see different results.

square()
addOne()

//addOne()
//square()

print(x)
/*:Now we will do the same thing using pure functions.*/
func functionalSquare(_ val: Int) -> Int {
    val * val
}

func functionalAdd(_ val: Int) -> Int {
    val + 1
}

print(functionalAdd(functionalSquare(10)))
print(functionalSquare(functionalAdd(10)))


/*:
 ___
 
 ### Is this a pure function?
 */
// Return a date with a given time offset
// Return nil if it is the first of the month
func getTimeOffset(withInterval interval: TimeInterval) -> Date? {
    let startTime = Date()
    
    let calendarComponent = Calendar.current.dateComponents([.day], from: startTime)
    
    if calendarComponent.day == 1 {
        return nil
    } else {
        return startTime.addingTimeInterval(interval)
    }
    
}

getTimeOffset(withInterval: TimeInterval(60))


//: [Next](@next)
